<?php
/**
 * @file
 * gg_news.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function gg_news_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'disqus';
  $context->description = 'Block for displaying Disqus comments';
  $context->tag = 'Social Media';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'article' => 'article',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'disqus-disqus_comments' => array(
          'module' => 'disqus',
          'delta' => 'disqus_comments',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Block for displaying Disqus comments');
  t('Social Media');
  $export['disqus'] = $context;

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gg_news';
  $context->description = 'Rovo News context';
  $context->tag = 'news';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'gg_news' => 'gg_news',
        'gg_news:page' => 'gg_news:page',
        'gg_news:page_1' => 'gg_news:page_1',
        'gg_news:page_2' => 'gg_news:page_2',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-gg_news_tags-block' => array(
          'module' => 'views',
          'delta' => 'gg_news_tags-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'tagadelic-1' => array(
          'module' => 'tagadelic',
          'delta' => 1,
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Rovo News context');
  t('news');
  $export['gg_news'] = $context;

  return $export;
}
